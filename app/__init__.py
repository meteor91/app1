from flask import g
from flask import Flask
from flask import render_template
from flask import request, redirect, url_for
from flask.ext.sqlalchemy import SQLAlchemy
from form.UserForm import UserForm
app = Flask(__name__)
app.config.from_pyfile('config.py')

db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)

    def __init__(self, username):
        self.username = username

    def __repr__(self):
        return self.username



@app.route("/")
def index():
    return "Hello world!"

@app.route("/user/new", methods=['POST', 'GET'])
def user_create():
    if request.method == 'POST':
        username = request.form['username']
        db.session.add(User(username))
        db.session.commit()
        return redirect((url_for('user_list')))
    else:
        return render_template('user/create.html', form=UserForm(csrf_enabled=False))

@app.route("/user/update/<id>", methods=['POST', 'GET'])
def user_update(id):
    user = User.query.get(id)
    if request.method == 'POST':
        user.username = request.form['username']
        #db.session.(User(username))
        db.session.commit()
        return redirect((url_for('user_list')))
    else:
        form = UserForm(csrf_enabled=False)
        form.username.data = user.username
        return render_template('user/update.html', form=form, id=id)

@app.route("/user/show")
def user_list():
    rv = User.query.order_by(User.id).all()
    return render_template('user/list.html', rv = rv)


@app.route("/hello")
@app.route("/hello/<name>")
def hello(name=None):
    return render_template('hello.html', name = name)

@app.route("/welcome")
def welcome():
    return "Hello world!"



if __name__ == "__main__":
    app.run()

