drop table if exists user;
create table user (
user_id integer primary key autoincrement,
username text not null
);

drop table if exists message;
create table message (
message_id integer primary key autoincrement,
author_id integer not null,
text text not null,
pub_date integer
);